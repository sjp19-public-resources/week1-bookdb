from django.shortcuts import render

from books.forms import BookForm
from books.models import Book

# Create your views here.
def show_books(request):

    # Test to see if it is a POST request
    if request.method == "POST":

        # Create the BookForm
        form = BookForm(request.POST)

        # Test the inputs (more on this to come...)
        if form.is_valid:

            # Tell the form to save the book
            form.save()

    # fetch all of the books from the database
    books = Book.objects.all()
    form = BookForm()
    context = {
        "books": books,
        "form": form,
    }
    return render(request, "home.html", context)

"""
 The view below is for the final discussion about django
"""

def search_books(request):

    # Test to see if it is a GET request
    if request.method == "GET":

        # get the "q" parameter out of the query-string
        query = request.GET.get("q")
        if query:

            # use filter() to filter the objects
            books = Book.objects.filter(title__icontains=query)
        else:
            books = Book.objects.all()
    else:
        books = Book.objects.all()
        

    form = BookForm()
    context = {
        "books": books,
        "form": form,
    }
    return render(request, "home2.html", context)


"""
The landing page
"""

# return a static html page
def index(request):
    return(render(request, "index.html"))